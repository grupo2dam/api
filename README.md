# API Batoipop

**Ruta principal 137.74.226.42:8080/**

# Guia
## Palabras entre *: datos a introducir, se deben de cambiar
## Sentencias con doble ": no hay que poner nada, se devuelve el resultado con la ruta base

## /articulo

---
    1. /all Todos los articulos
    2. /id?id=*id* recibir el articulo con esa id
    3. /nombre?n=*nombre* recibir todos los articulos que contengan ese conjunto de caracteres en su nombre
    4. /usuario?id=*id* devuelve tel usuario que ha publicado ese articulo
    5. /etiqueta?id=*id* devuelve todas las etiquetas del articulo


## /categoria

---
    1. /all Devuelve todas las categorias


## /empleado

---
    1. ?u=*correoDeUsuario*&p=*contraseñaEncriptada* Devuelve un usuario si el login es correcto


## /etiqueta

---
    1. /all Devuelve todas las etiquetas

## /favorito

---
    1. =id=*idFavorito* Devuelve una entrada de la tabla favorito en concreto


## /mensajes

---
    1. ?id1=*idUsuario1*&id2=*idUsuario 2* devuelve toda la lista de mensajes entre 2 usuarios
    2. /last?id1=*idUsuario1*&id2=*idUsuario 2* devuelve el ultimo mensaje de el primer usuario al segundo usuario



## /usuario

 ---
    1. /all Todos los usuarios
    2. ?id=*id* recibir usuario por id
    3. /login?u=*usuario*&p=*contraseña* veerificar que el login es correcto
    4. /exists?u=*usuario* Comprobar si el nombre de un usuario existe  
  
## /valoracion

---
    1. "" Devuelve todas las valoraciones
    2. /usuario?id=*idUsuario* Deuelve las valoraciones del usuario

