package es.batoipop.batoipopApi.Repository;

import es.batoipop.batoipopApi.pojos.BatoipopCategoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaRepository extends JpaRepository<BatoipopCategoria, Integer> {
}
