package es.batoipop.batoipopApi.Repository;

import es.batoipop.batoipopApi.pojos.BatoipopEtiqueta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtiquetasRepository extends JpaRepository<BatoipopEtiqueta, Integer> {
}
