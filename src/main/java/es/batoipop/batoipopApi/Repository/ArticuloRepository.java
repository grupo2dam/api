package es.batoipop.batoipopApi.Repository;

import es.batoipop.batoipopApi.pojos.BatoipopArticulo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticuloRepository extends JpaRepository<BatoipopArticulo, Integer> {
}
