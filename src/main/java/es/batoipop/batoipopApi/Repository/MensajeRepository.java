package es.batoipop.batoipopApi.Repository;

import es.batoipop.batoipopApi.pojos.BatoipopMensajeUsuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MensajeRepository extends JpaRepository<BatoipopMensajeUsuario, Integer> {

    @Query("Select m from BatoipopMensajeUsuario m where (m.batoipopUsuarioByUsuarioEmisor.id = ?1 and m.batoipopUsuarioByUsuarioReceptor.id = ?2 ) or (m.batoipopUsuarioByUsuarioEmisor.id = ?2 and m.batoipopUsuarioByUsuarioReceptor.id = ?1 ) ")
    List<BatoipopMensajeUsuario> findMessagesBetweenUsers(Integer id1, Integer id2 );
}
