package es.batoipop.batoipopApi.Repository;

import es.batoipop.batoipopApi.pojos.BatoipopValoracion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ValoracionRepository extends JpaRepository<BatoipopValoracion, Integer> {

    @Query("Select v from BatoipopValoracion v where v.batoipopUsuarioByUsuarioValorado.id = ?1")
    public List<BatoipopValoracion> findByUser(Integer id);
}
