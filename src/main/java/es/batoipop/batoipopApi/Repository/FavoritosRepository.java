package es.batoipop.batoipopApi.Repository;

import es.batoipop.batoipopApi.pojos.BatoipopFavorito;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FavoritosRepository extends JpaRepository<BatoipopFavorito, Integer> {

    @Query("Select f from BatoipopFavorito f where f.batoipopUsuario.id = ?1")
    public List<BatoipopFavorito> favoritesByUser(Integer id);
}
