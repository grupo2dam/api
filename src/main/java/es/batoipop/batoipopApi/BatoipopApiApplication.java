package es.batoipop.batoipopApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatoipopApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatoipopApiApplication.class, args);
	}

}
