package es.batoipop.batoipopApi.Controller;

import es.batoipop.batoipopApi.Service.EtiquetaService;
import es.batoipop.batoipopApi.pojos.BatoipopEtiqueta;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/etiqueta")
public class EtiquetaController {
    @Autowired
    private EtiquetaService etiquetaService;

    @GetMapping("/all")
    public List<BatoipopEtiqueta> getAll(){
        return etiquetaService.getAll();
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopEtiqueta et){
        BatoipopEtiqueta etiqNueva = etiquetaService.saveUsuario(et);
        return new ResponseEntity<>(etiqNueva.getId(), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            etiquetaService.deleteEtiqueta(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


}
