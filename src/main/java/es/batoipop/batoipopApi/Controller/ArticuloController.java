package es.batoipop.batoipopApi.Controller;

import es.batoipop.batoipopApi.Service.ArticuloService;
import es.batoipop.batoipopApi.pojos.BatoipopArticulo;
import es.batoipop.batoipopApi.pojos.BatoipopEtiqueta;
import es.batoipop.batoipopApi.pojos.BatoipopUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/articulo")
public class ArticuloController {
    @Autowired
    ArticuloService articuloService;

    @GetMapping("/all")
    public List<BatoipopArticulo> list() {
        return articuloService.listAllArticulos();
    }

    @GetMapping("/id")
    public ResponseEntity<BatoipopArticulo> get(@RequestParam(required = true, defaultValue = "1") int id) {
        try {
            BatoipopArticulo bArticulo = articuloService.getArticulo(id);
            return new ResponseEntity<BatoipopArticulo>(bArticulo, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<BatoipopArticulo>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/nombre")
    public List<BatoipopArticulo> getByNombre(@RequestParam(required = true, defaultValue = "") String n) {
        return articuloService.getArticulosName(n);
    }

    @GetMapping("/usuario")
    public List<BatoipopUsuario> getUsuario(@RequestParam Integer id){
        return articuloService.getUser(id);
    }

    @GetMapping("/etiqueta")
    public List<BatoipopEtiqueta> getEtiquetas(@RequestParam Integer id){
        return articuloService.getEtiquetas(id);
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopArticulo art) {
        BatoipopArticulo artNuevo = articuloService.saveArticulo(art);
        return new ResponseEntity<>(artNuevo.getId(), HttpStatus.CREATED);
    }
}
