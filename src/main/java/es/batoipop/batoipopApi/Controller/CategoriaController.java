package es.batoipop.batoipopApi.Controller;

import es.batoipop.batoipopApi.Service.CategoriaService;
import es.batoipop.batoipopApi.pojos.BatoipopCategoria;
import es.batoipop.batoipopApi.pojos.BatoipopUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/categoria")
public class CategoriaController {
    @Autowired
    private CategoriaService categoriaService;

    @GetMapping("/all")
    public List<BatoipopCategoria> all(){
        return categoriaService.findAll();
    }

    @GetMapping("")
    public ResponseEntity<BatoipopCategoria> get(@RequestParam(required = true, defaultValue = "1") int id){
        try{
            BatoipopCategoria bUsuario = categoriaService.getCategoria(id);
            return new ResponseEntity<BatoipopCategoria>(bUsuario, HttpStatus.OK);
        }catch (NoSuchElementException e){
            return new ResponseEntity<BatoipopCategoria>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopCategoria cat){
        BatoipopCategoria catNueva = categoriaService.saveCategoria(cat);
        return new ResponseEntity<>(catNueva.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopCategoria category, @PathVariable Integer id){
        try{
            BatoipopCategoria cat = categoriaService.getCategoria(id);
            cat.setNombre(category.getNombre());    
            cat.setDescripcion(category.getDescripcion());
            categoriaService.saveCategoria(cat);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (NoSuchElementException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            categoriaService.deleteCategoria(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
