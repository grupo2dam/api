package es.batoipop.batoipopApi.Controller;

import es.batoipop.batoipopApi.Service.UsuarioService;
import es.batoipop.batoipopApi.pojos.BatoipopUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    @Autowired
    UsuarioService usuarioService;

    @GetMapping("/all")
    public List<BatoipopUsuario> list() {
        return usuarioService.listAllUsuarios();
    }

    @GetMapping("")
    public ResponseEntity<BatoipopUsuario> get(@RequestParam(required = true, defaultValue = "1") int id){
        try{
            BatoipopUsuario bUsuario = usuarioService.getCliente(id);
            return new ResponseEntity<BatoipopUsuario>(bUsuario, HttpStatus.OK);
        }catch (NoSuchElementException e){
            return new ResponseEntity<BatoipopUsuario>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/login")
    public ResponseEntity<BatoipopUsuario> get(@RequestParam(required = true, defaultValue = "1") String u, @RequestParam(required = true, defaultValue = "1") String p){
        try{
            BatoipopUsuario bUsuario = usuarioService.checkUser(u,p);
            return new ResponseEntity<BatoipopUsuario>(bUsuario, HttpStatus.OK);
        }catch (NoSuchElementException e){
            return new ResponseEntity<BatoipopUsuario>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/exists")
    public ResponseEntity<BatoipopUsuario> get(@RequestParam(required = true, defaultValue = "1") String u){
        try{
            BatoipopUsuario bUsuario = usuarioService.checkUserExists(u);
            return new ResponseEntity<BatoipopUsuario>(bUsuario, HttpStatus.OK);
        }catch (NoSuchElementException e){
            return new ResponseEntity<BatoipopUsuario>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopUsuario usu) {
        BatoipopUsuario usuNuevo = usuarioService.saveUsuario(usu);
        return new ResponseEntity<>(usuNuevo.getId(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody BatoipopUsuario user, @PathVariable Integer id){
        try{
            BatoipopUsuario us = usuarioService.getCliente(id);
            us.setNombre(user.getNombre());
            us.setContrasena(user.getContrasena());
            us.setEmail(user.getEmail());
            us.setTelefono(user.getTelefono());
            usuarioService.saveUsuario(us);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (NoSuchElementException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        try {
            usuarioService.deleteUsuario(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
