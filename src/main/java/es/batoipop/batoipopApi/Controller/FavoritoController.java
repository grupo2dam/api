package es.batoipop.batoipopApi.Controller;

import es.batoipop.batoipopApi.Service.FavoritoService;
import es.batoipop.batoipopApi.pojos.BatoipopArticulo;
import es.batoipop.batoipopApi.pojos.BatoipopFavorito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/favorito")
public class FavoritoController {
    @Autowired
    private FavoritoService favoritoService;

    @GetMapping
    public List<BatoipopFavorito> get(@RequestParam Integer id){
        return favoritoService.getUserFavorites(id);
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopFavorito favorito) {
        BatoipopFavorito favNuevo = favoritoService.saveArticulo(favorito);
        return new ResponseEntity<>(favNuevo.getId(), HttpStatus.CREATED);
    }
}
