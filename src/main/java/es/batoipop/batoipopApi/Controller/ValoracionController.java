package es.batoipop.batoipopApi.Controller;

import es.batoipop.batoipopApi.Service.ValoracionService;
import es.batoipop.batoipopApi.pojos.BatoipopValoracion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/valoracion")
public class ValoracionController {
    @Autowired
    private ValoracionService valoracionService;

    @GetMapping("")
    public List<BatoipopValoracion> getAll(){
        return valoracionService.findAll();
    }

    @GetMapping("/usuario")
    public List<BatoipopValoracion> getByUser(@RequestParam Integer id){
        return valoracionService.findByUser(id);
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopValoracion val){
        return new ResponseEntity<>(valoracionService.add(val).getId(), HttpStatus.CREATED);
    }

}
