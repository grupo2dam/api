package es.batoipop.batoipopApi.Controller;

import es.batoipop.batoipopApi.Service.EmpleadosService;
import es.batoipop.batoipopApi.pojos.BatoipopEmpleados;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/empleado")
public class EmpleadosController {
    @Autowired
    EmpleadosService empleadosService;

    @GetMapping("")
    public ResponseEntity<?> login(@RequestParam String u, @RequestParam String p){
        BatoipopEmpleados empleados = empleadosService.login(u,p);
        if (empleados == null){
            return new ResponseEntity<>(empleados, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(empleadosService.login(u,p),HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopEmpleados emp){
        BatoipopEmpleados emplNuevo = empleadosService.add(emp);
        return new ResponseEntity<>(emplNuevo.getId(), HttpStatus.CREATED);
    }
}
