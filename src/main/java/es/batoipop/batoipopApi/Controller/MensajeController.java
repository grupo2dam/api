package es.batoipop.batoipopApi.Controller;

import es.batoipop.batoipopApi.Service.MensajeService;
import es.batoipop.batoipopApi.pojos.BatoipopMensajeUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/mensajes")
public class MensajeController {
    @Autowired
    MensajeService mensajeService;

    @GetMapping("")
    public List<BatoipopMensajeUsuario> get(@RequestParam(required = true) int id1, @RequestParam(required = true) int id2){
        return mensajeService.getMensajesBetweenUsers(id1, id2);
    }

    @GetMapping("/last")
    public BatoipopMensajeUsuario getLast(@RequestParam(required = true) int id1, @RequestParam(required = true) int id2){
        return mensajeService.getLastMessage(id1, id2);
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody BatoipopMensajeUsuario bmu){
        BatoipopMensajeUsuario mensNuevo = mensajeService.saveMensaje(bmu);
        return new ResponseEntity<>(mensNuevo.getId(), HttpStatus.CREATED);
    }
}
