package es.batoipop.batoipopApi.Service;

import es.batoipop.batoipopApi.Repository.FavoritosRepository;
import es.batoipop.batoipopApi.pojos.BatoipopFavorito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class FavoritoService {
    @Autowired
    private FavoritosRepository favoritosRepository;

    public List<BatoipopFavorito> getUserFavorites(Integer id) {
        return favoritosRepository.favoritesByUser(id);
    }

    public BatoipopFavorito saveArticulo(BatoipopFavorito favorito) {
        return favoritosRepository.save(favorito);
    }
}
