package es.batoipop.batoipopApi.Service;

import javax.transaction.Transactional;

import es.batoipop.batoipopApi.Repository.UsuarioRepository;
import es.batoipop.batoipopApi.pojos.BatoipopUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@Transactional
public class UsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    public List<BatoipopUsuario> listAllUsuarios(){
        return usuarioRepository.findAll();
    }

    public BatoipopUsuario getCliente(Integer id) throws NoSuchElementException{
        return usuarioRepository.findById(id).get();
    }

    public BatoipopUsuario saveUsuario(BatoipopUsuario bu) {
        bu = usuarioRepository.save(bu);
        return bu;
    }

    public void deleteUsuario(Integer id){
        usuarioRepository.deleteById(id);
    }

    public BatoipopUsuario checkUser(String u, String p){
        List<BatoipopUsuario> list = listAllUsuarios();

        for (BatoipopUsuario batoipopUsuario : list) {
            if (batoipopUsuario.getNombre().equalsIgnoreCase(u) && batoipopUsuario.getContrasena().equals(p)){
                return batoipopUsuario;
            }
        }
        throw new NoSuchElementException();
    }

    public BatoipopUsuario checkUserExists(String u){
        List<BatoipopUsuario> list = listAllUsuarios();

        for (BatoipopUsuario batoipopUsuario : list) {
            if (batoipopUsuario.getNombre().equalsIgnoreCase(u)){
                return batoipopUsuario;
            }
        }
        throw new NoSuchElementException();
    }
}
