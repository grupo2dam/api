package es.batoipop.batoipopApi.Service;

import es.batoipop.batoipopApi.Repository.ValoracionRepository;
import es.batoipop.batoipopApi.pojos.BatoipopValoracion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ValoracionService {
    @Autowired
    private ValoracionRepository valoracionRepository;

    public List<BatoipopValoracion> findAll() {
        return valoracionRepository.findAll();
    }

    public List<BatoipopValoracion> findByUser(Integer id) {
        return valoracionRepository.findByUser(id);
    }

    public BatoipopValoracion add(BatoipopValoracion val) {
        return valoracionRepository.save(val);
    }
}
