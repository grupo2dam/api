package es.batoipop.batoipopApi.Service;

import es.batoipop.batoipopApi.Conexion;
import es.batoipop.batoipopApi.Repository.MensajeRepository;
import es.batoipop.batoipopApi.pojos.BatoipopMensajeUsuario;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MensajeService {
    @Autowired
    private MensajeRepository mensajeRepository;

    public List<BatoipopMensajeUsuario> getMensajesBetweenUsers(Integer id1, Integer id2) {
        return mensajeRepository.findMessagesBetweenUsers(id1, id2);
    }

    public BatoipopMensajeUsuario getLastMessage(Integer id1, Integer id2){
        List<BatoipopMensajeUsuario> list = mensajeRepository.findAll();
        BatoipopMensajeUsuario last = null;

        for (BatoipopMensajeUsuario batoipopMensajeUsuario : list) {
            if (batoipopMensajeUsuario.getBatoipopUsuarioByUsuarioEmisor().getId() == id1 && batoipopMensajeUsuario.getBatoipopUsuarioByUsuarioReceptor().getId() == id2 ){
                last =  batoipopMensajeUsuario;
            }
        }
        return last;
    }

    public BatoipopMensajeUsuario saveMensaje(BatoipopMensajeUsuario bmu) {
        bmu = mensajeRepository.save(bmu);
        return bmu;
    }
}

