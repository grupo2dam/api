package es.batoipop.batoipopApi.Service;

import es.batoipop.batoipopApi.Repository.ArticuloRepository;
import es.batoipop.batoipopApi.Repository.UsuarioRepository;
import es.batoipop.batoipopApi.pojos.BatoipopArticulo;
import es.batoipop.batoipopApi.pojos.BatoipopEtiqueta;
import es.batoipop.batoipopApi.pojos.BatoipopUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class ArticuloService {
    @Autowired
    private ArticuloRepository articuloRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    public List<BatoipopArticulo> listAllArticulos(){
        return articuloRepository.findAll();
    }

    public BatoipopArticulo getArticulo(Integer id){
        return articuloRepository.findById(id).get();
    }

    public BatoipopArticulo saveArticulo(BatoipopArticulo ba){
        ba = articuloRepository.save(ba);
        return ba;
    }

    public List<BatoipopArticulo> getArticulosName(String nombre){
        nombre = nombre.toLowerCase(Locale.ROOT);
        String nombreActual;
        List<BatoipopArticulo> articulos = listAllArticulos();
        List<BatoipopArticulo> similarName = new ArrayList<>();
        for (BatoipopArticulo articulo : articulos) {
            nombreActual = articulo.getNombre().toLowerCase(Locale.ROOT);
            if (nombreActual.contains(nombre)){
                similarName.add(articulo);
            }
        }
        return similarName;
    }

    public List<BatoipopUsuario> getUser(Integer id){
        List<BatoipopUsuario> list = new ArrayList<>();
        list.add(usuarioRepository.findById(articuloRepository.findById(id).get().getUsuarioPublicar().getId()).get());
        return list;
    }

    public List<BatoipopEtiqueta> getEtiquetas(Integer id) {
        BatoipopArticulo art = getArticulo(id);
        return (List<BatoipopEtiqueta>) art.getBatoipopEtiquetas();
    }
}
