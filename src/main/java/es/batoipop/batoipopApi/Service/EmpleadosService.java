package es.batoipop.batoipopApi.Service;

import es.batoipop.batoipopApi.Repository.EmpleadosRepository;
import es.batoipop.batoipopApi.pojos.BatoipopEmpleados;
import es.batoipop.batoipopApi.pojos.ResUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EmpleadosService {
    @Autowired
    private EmpleadosRepository empleadosRepository;

    public BatoipopEmpleados login(String username, String password){
        List<BatoipopEmpleados> users = empleadosRepository.findAll();

        for (BatoipopEmpleados user : users) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    }

    public BatoipopEmpleados add(BatoipopEmpleados emp) {
        emp = empleadosRepository.save(emp);
        return emp;
    }
}
