package es.batoipop.batoipopApi.Service;

import es.batoipop.batoipopApi.Repository.CategoriaRepository;
import es.batoipop.batoipopApi.pojos.BatoipopCategoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@Transactional
public class CategoriaService {
    @Autowired
    private CategoriaRepository categoriaRepository;

    public List<BatoipopCategoria> findAll() {
        return categoriaRepository.findAll();
    }

    public BatoipopCategoria getCategoria(Integer id) throws NoSuchElementException {
        return categoriaRepository.findById(id).get();
    }

    public BatoipopCategoria saveCategoria(BatoipopCategoria cat){
        cat = categoriaRepository.save(cat);
        return cat;
    }

    public void deleteCategoria(Integer id) {
        categoriaRepository.deleteById(id);
    }

}
