package es.batoipop.batoipopApi.Service;

import es.batoipop.batoipopApi.Repository.EtiquetasRepository;
import es.batoipop.batoipopApi.pojos.BatoipopEtiqueta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EtiquetaService {
    @Autowired
    private EtiquetasRepository etiquetasRepository;

    public List<BatoipopEtiqueta> getAll() {
        return etiquetasRepository.findAll();
    }

    public BatoipopEtiqueta saveUsuario(BatoipopEtiqueta et) {
        et = etiquetasRepository.save(et);
        return et;
    }

    public void deleteEtiqueta (int id) {
        etiquetasRepository.deleteById(id);
    }
}
