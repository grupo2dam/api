package es.batoipop.batoipopApi;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Conexion {
    // A SessionFactory is set up once for an application!
    private static Session sesion;

    public static Session getSession() {
        // configure() -> configures settings from hibernate.cfg.xml
        if (sesion == null) {
            StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
            try {
                SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
                sesion = sessionFactory.openSession();
            } catch (Exception e) {
                // The registry would be destroyed by the SessionFactory,
                // but we had trouble building the SessionFactory -> so destroy it manually.
                StandardServiceRegistryBuilder.destroy(registry);
            }
        }
        return sesion;
    }

    public static void closeSession() {
        if (sesion != null) {
            sesion.close();
            sesion = null;
        }
    }
}
